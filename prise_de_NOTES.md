# ~~~ Prise de notes ~~~

## DÉBUT DU PROJET TUTORÉ

On veut une précision de moins de 10 mètres avec le RFID.
Ils ont déjà un repérage avec cartographie, mais le GPS a une faible précision à moins de dix mètres.
--> Si possible descendre à 1 mètre de précision
Obligation : étiquette passive, discrète et pérenne (M. Faucon : sol)



## CONSEILS M. TAHIRY

Quel est le probleme ?
Pourquoi c'est un problème ?
Pourquoi ce problème est intéressant ?

1) Expliquer le problème

2) Expliquer la solution

3) Dire que l'on a fait la partie la plus importante : la réflexion

==> SAVOIR VENDRE SON PRODUIT / SES RÉALISATIONS




## ÇA C'EST VRAI...

Attention le tag peut être obstrué par un caillou. Est-ce qu'on peut toujours le lire ?




## 1er RENDEZ-VOUS AVEC M. ARNOUX

Chargée de mission de géolocalisation

Mme Poungavanon responsable scientifique, lutte pour conserver les especes endemiques + directeur du CENR seront présents à la soutenance de juin.

Le CENR a beaucoup perdu, ils sont inquiets de la situation



## ÉQUIPEMENT DU CENR

Tablettes utilisées pour les relevés :
SAMSUNG GALAXY Tab A3 modèle SM-T280
Android Lollipop 5.1.1

Autres tablettes :
SAMSUNG Galaxy Tab 2 modèle GT-P3110
Android 4.1.2

Les tablettes qui nous ont été prêtées :
Samsung Galaxy Tab 3 modèle GT-P5210
Android 4.4.2

Smartphone Samsung : Galaxy Note 3
Modèle SM-N9005
Android 5.0



## :-( :-( :-( DENIER RENDEZ-VOUS, FIN DU PT :-(:-(:-(


==> JUSQU'A LA FIN
Partir sur l'aspect software.
Terminer la fonction READ et développer la fonction WRITE.
Essayer de rendre un outil fonctionnel au CENR.


## Découpage par sites

### Conventions de nommage pour la fonction WRITE :

Lieu (4 caratere)
Espèce (6 caracteres)
Numéro du lot (3 carateres)
Numero de l'espece (3 carateres)

### Découpage par site

code en 4 lettres 

	ex SASO
	ex HDMV
	EX grch
	14 sites

+3 première lettres du genre

+3 premières lettre de l'éspèce

+ numéro individu (commence à 1) (peut dépasser 1000) (mettre 5 chiffres min)


forme tag (clou) (sur lequel on peut écrire)

impression antenne en cours

quelle profondeur?
	
BILAN
	regarder taille adresse
	antenne à tracer
	échantillons à récolter (demander au CENR) (sec, humide, paillage, feuillage)

L'inscription sur la carte
	permettre l'inscription depuis l'application
	sensible à la casse (en fait) TOUT EN MINISCULE
	volonté d'automatiser l'écriture


## Clous RFID D-Think :

ISO 15693
Model : D-Think_T01-HF

Fréquence : 13.56 MHz
Températures de fonctionnement : -20℃   -   90℃
Durée de vie : + de 10 ans

Distance max de lecture : 20mm - 50cm
