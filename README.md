<details>
<summary>[ Français ]</summary>

# Identification et Repérage de Plantes en Milieu Naturel

Bienvenue !

```
              |
            \ _ /
          -= (_) =-                 
            /   \                  ,~                             _\/_                             ,\//,  .\//\/.
              |                    |\                             //o\   _\/_             ,.,.,    //o\\  /o//o\\
    _  ___  __  _ __ __ _  _ __ _ /| \ __ __ _ _ _ _ __ _ __ _ __ _ | __ /o\\ _          /###/#\     |     |  |
  =-=-_=-=-_=-=_=-_=_=-_=-_-_=_=-/_|__\_=-=-==_==--==__==_==_=-=_,-'|"'""-|-,_ ^^^^^^^^^^|' '|:| ^^^^|^^^^^|^^|^^^^
   =- _=-=-_=- _=-= _--_ =-= -_=-=_-=__=- _=-= _--_ =-=,-"          |^^^^^|^^^^^^^^^^^^^^^^^^^^^^^^^^|`=.='|^^|^^^^
     =- =- =-= =- = -  -===- -= -= -===- =- =- -===-."   jgs  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

```

### Objectifs

Intitulé du projet : **"Conception et réalisation d’un outil d’identification et de repérage de plantes endémiques et indigènes en milieu naturel"**.

L'objectif de ce projet est de trouver une solution technologique simple, fiable, durable et discrète qui réponde aux besoins exprimés par le CENR. Pour plus de détails veuillez lire [le rapport final de Projet Tutoré](https://gitlab.com/prsunflower/treenity/blob/master/Realisations/RAPPORT_DE_PROJET_TUTORE.pdf)


### Contenu du dépot Git

 - **[Code source de l'application Android](https://gitlab.com/prsunflower/treenity/tree/master/Realisations/app)**
 
 - [Documentation du lecteur RFID __*DLP-RFID2D USB Dongle*__](https://gitlab.com/prsunflower/treenity/-/blob/b0613293e68d34cb5e7c5877dbbf92d0696f49a8/Documentation/Lecteur_RFID/DLP-RFID2-Documentation-Lecteur-ds-v10-215391.pdf)

 - [Commandes RFID selon la norme ISO 15693](https://gitlab.com/prsunflower/treenity/-/blob/b0613293e68d34cb5e7c5877dbbf92d0696f49a8/Documentation/Lecteur_RFID/sloa141-ISO_15693_Host_Commands.pdf)
 
 - [Manuel d'Utilisation du logiciel __*Texas Instruments TRF7970A Evaluation Module (EVM)*__](https://gitlab.com/prsunflower/treenity/-/blob/b0613293e68d34cb5e7c5877dbbf92d0696f49a8/Documentation/Lecteur_RFID/Command_strings_Texas_I_SLOU321a.pdf)
 
 - [Explications de la démarche suivie pour mener à bien le projet](https://gitlab.com/prsunflower/treenity/blob/master/Realisations/RAPPORT_DE_PROJET_TUTORE.pdf)


### Matériel requis

Pour pouvoir utiliser l'application il faut :
- 1x appareil Android fonctionnant sous **v4.4 KitKat (API 19) au minimum**
- 1x lecteur RFID du type DLP-RFID2 Dongle branché par USB à la tablette/smartphone
- Un ou plusieurs tags RFID compatibles norme **ISO 15693**
- *Optionnel :* une antenne externe pour augmenter la portée


## Mise en place et utilisation

### Branchements

1. Planter les tags RFID aux pieds des arbres à identifier
2. Visser le fil de connexion de l'antenne du lecteur au connecteur de l'antenne externe
3. Brancher le lecteur RFID USB à un adaptateur USB OTG pour le relier à l'appareil Android
4. Brancher le lecteur RFID à l'appareil Android
5. Ouvrir l'application Android et accepter la demande d'autorisation qui s'affiche à l'écran lors du branchement du lecteur

### Lire un identifiant

1. Placer l'antenne du lecteur RFID au plus proche du clou RFID à lire. Les résultats sont optimaux quand la surface de l'antenne touche la tête du clou RFID  (moins de 2 cm)
2. Lancer la lecture en appuyant sur le bouton "**SCANNER**" (un bouton grisé indique que la lecture est en cours ou que le lecteur RFID USB est mal branché)
3. Si la lecture s'est bien déroulée, l'identifiant de l'arbre s'inscrit à l'écran
4. Sinon, le message "**ERREUR DE LECTURE**" s'affiche. Réessayez en vous approchant plus du clou RFID et en faisant en sorte que la surface soit perpendiculaire au clou
5. Si la lecture n'affiche rien à l'écran, cela signifie qu'un tag a été détecté mais qu'il est vide, il faut donc en écrire un identifiant (voir plus bas "**Écrire un identifiant**" ) 

### Copier un identifiant

Après avoir lu d'identifiant, l'utilisateur peut le copier dans l'application de suivi de plantation :

1. Une fois l'identifiant visible à l'écran, cliquer sur "**COPIER**"
2. Quitter l'application pour ouvrir l'application de suivi de plantation (par exemple "Cyber Tracker"). Appuyer longtemps sur la zone de texte où l'utilisateur souhaite coller l'identifiant qui vient d'être lu, toucher "**Coller**"


### Écrire un identifiant

1. Placer l'antenne du lecteur RFID au plus proche du clou RFID
2. Dans la partie "**Écrire Identifiant**" de l'application, compléter les différents champs texte en suivant le modéle pré-inscrit dans ceux-ci

***Important :*** ÉCRIRE EN MAJUSCULES

4. Veiller à saisir le bon nombre de caractères dans les champs texte. Si un champ texte comporte trop ou pas assez de caractères l'application affichera un message d'erreur.
5. Appuyer sur le bouton "**ECRIRE**" pour lancer l'écriture. Le bouton est grisé quand l'écriture est en cours.





## À faire

Voir : [les "issues" treenity](https://gitlab.com/prsunflower/treenity/boards)


## Auteurs

**Groupe 'Treenity', étudiants à l'IUT de La Réunion (2016-18) :**

 * M. Aviran TETIA
 * M. Alexandre YOUNG LAN SUN
 * Mme. Marie REQUENA
 * Mme. Anaïs MARDAMA NAYAGOM

Pour l’association à but non lucratif **Conservatoire d’espaces naturels de la Réunion (CENR)**


## Liens

[Qui sont les Conservatoires d’espaces naturels (CEN) ?](http://www.reseau-cen.org/fr/decouvrir-le-reseau/qui-sont-les-conservatoires-d-espaces-naturels)

[Site web du CEN de la Réunion (CENR)](http://www.reseau-cen.org/fr/actualites-agenda/conservatoire-d-espaces-naturels-de-la-reunion-de-drones-de-sites)

[Documentation lecteur RFID *DLP Design* :](http://www.dlpdesign.com/rf/rfid2.php)


## Contact

**[Aviran TÉTIA](mailto:a.tetia@rt-iut.re)**, Gitlab : https://gitlab.com/Aviran

**[Stéphane ARNOUX](mailto:s.arnoux@gceip.fr)**



> Notre logo a été créé à partir de ces 2 images : [lien 1](https://openclipart.org/detail/263892/colorful-natural-tree) et [lien 2](http://www.mindfulstreams.com/wp-content/uploads/2015/08/rfid.png)


</details>




























<details>
<summary>[ English ]</summary>

# Identifying Trees for Management Purposes

Welcome !

```
              |
            \ _ /
          -= (_) =-                 
            /   \                  ,~                             _\/_                             ,\//,  .\//\/.
              |                    |\                             //o\   _\/_             ,.,.,    //o\\  /o//o\\
    _  ___  __  _ __ __ _  _ __ _ /| \ __ __ _ _ _ _ __ _ __ _ __ _ | __ /o\\ _          /###/#\     |     |  |
  =-=-_=-=-_=-=_=-_=_=-_=-_-_=_=-/_|__\_=-=-==_==--==__==_==_=-=_,-'|"'""-|-,_ ^^^^^^^^^^|' '|:| ^^^^|^^^^^|^^|^^^^
   =- _=-=-_=- _=-= _--_ =-= -_=-=_-=__=- _=-= _--_ =-=,-"          |^^^^^|^^^^^^^^^^^^^^^^^^^^^^^^^^|`=.='|^^|^^^^
     =- =- =-= =- = -  -===- -= -= -===- =- =- -===-."   jgs  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

```


### Objectives

The purpose of this project is to provide a technological solution which suits the needs expressed by the CENR. The core aspects of our solution are : reliability, ease of use, toughness and discretion. For more information please read [the final report of the project](https://gitlab.com/prsunflower/treenity/blob/master/Realisations/RAPPORT_DE_PROJET_TUTORE.pdf)


### Contents of the Git repository

 - **[Source code of the Android app](https://gitlab.com/prsunflower/treenity/tree/master/Realisations/app)**
 
 - [RFID reader/writer documentation __*DLP-RFID2D USB Dongle*__](https://gitlab.com/prsunflower/treenity/-/blob/b0613293e68d34cb5e7c5877dbbf92d0696f49a8/Documentation/Lecteur_RFID/DLP-RFID2-Documentation-Lecteur-ds-v10-215391.pdf)

 - [RFID commands according to ISO 15693](https://gitlab.com/prsunflower/treenity/-/blob/b0613293e68d34cb5e7c5877dbbf92d0696f49a8/Documentation/Lecteur_RFID/sloa141-ISO_15693_Host_Commands.pdf)
 
 - [User Guide for __*Texas Instruments TRF7970A Evaluation Module (EVM)*__](https://gitlab.com/prsunflower/treenity/-/blob/b0613293e68d34cb5e7c5877dbbf92d0696f49a8/Documentation/Lecteur_RFID/Command_strings_Texas_I_SLOU321a.pdf)
 
 - [How we managed to complete this project](https://gitlab.com/prsunflower/treenity/blob/master/Realisations/RAPPORT_DE_PROJET_TUTORE.pdf)
 
 

### Dependencies / Hardware / Software

In order to run the app we need :
- 1x Android device with **at minimum v4.4 KitKat (API 19)**
- 1x RFID USB Dongle like DLP-RFID2 plugged into the tablet/smartphone by USB
- One or more RFID tags which respect norm **ISO 15693**
- *Optionnal :* an external antenna which increases the reading/writing range


## How to use

### Preparing the equipment

1. Plant the RFID tags at the base of the trees that need to be identified
2. Screw the connection wire of the reader's antenna into the external antenna connector
3. Plug the USB RFID reader into a USB 'On-The-Go' adapter to connect it to the Android device
4. Plug the RFID reader into the Android device
5. Open the Android app, an authorisation request will pop up after the the reader has been plugged in. Accept it


### Reading an ID

1. Place the antenna of the RFID reader as close as possible to the RFID nail you want to read. The best results can be obtained when the surface of the antenna touches the head of the RFID tag (less than 2 cm)
2. Start reading by pressing the "**SCANNER**" button (a greyed-out button indicates that reading is in progress or that the USB RFID reader isn't correctly plugged in)
3. If the reading went well, the tree identifier will appear on the screen
4. Otherwise a "**ERREUR DE LECTURE**" message is displayed. Please try again by moving closer to the RFID nail and make sure the surface is perpendicular to the nail.
5. If nothing shows up on the screen, it means that a tag has been detected but it is empty. You can write an ID into the tag (see below "**Writing an ID**" ).


### Copying the ID

After reading the ID, the user may copy & paste it into the plant monitoring application:

1. Once the ID is visible on the screen, click on "**COPIER**".
2. Close the app and open the plant monitoring app (e.g. "Cyber Tracker"). Press and hold down the text box where the user wishes to paste the ID that has just been read, select "**Paste**".


### Writing an ID

1. Place the antenna of the RFID reader as close as possible to the RFID nail
2. In the "**Écrire Identifiant**" section, fill in the various text fields as needed

__*Important:*__  PLEASE WRITE IN ALL CAPS

3. Please input the correct number of characters in the text fields. If the chacracter count is incorrect the app will show an error message.
4. Once filled in press on the "**ECRIRE**" button to start writing. This button is greyed-out when writing is in progress



## Todos

See here : [Treenity "isues"](https://gitlab.com/prsunflower/treenity/boards)


## Authors

**Students of IUT de La Réunion (2016-18):**

 * Mr. Aviran TETIA
 * Mr. Alexandre YOUNG LAN SUN
 * Ms. Marie REQUENA
 * Ms. Anaïs MARDAMA NAYAGOM

Made for the non-profit organisation **Conservatoire d’espaces naturels de la Réunion (CENR)**


## Links

[Who are the 'Conservatoires d’espaces naturels' (CEN) ?](http://www.reseau-cen.org/fr/decouvrir-le-reseau/qui-sont-les-conservatoires-d-espaces-naturels)

[CEN de la Réunion's official website (CENR)](http://www.reseau-cen.org/fr/actualites-agenda/conservatoire-d-espaces-naturels-de-la-reunion-de-drones-de-sites)

[*DLP Design* RFID reader/writer documentation :](http://www.dlpdesign.com/rf/rfid2.php)


## Contact

**[Aviran TÉTIA](mailto:a.tetia@rt-iut.re)**, Gitlab : https://gitlab.com/Aviran

**[Stéphane ARNOUX](mailto:s.arnoux@gceip.fr)**


> Our logo was made from these pictures : [link 1](https://openclipart.org/detail/263892/colorful-natural-tree) and [link 2](http://www.mindfulstreams.com/wp-content/uploads/2015/08/rfid.png)



</details>
