package com.treenity.plantid;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent; //importé manuellement
import android.content.IntentFilter; //importé manuellement
import android.hardware.usb.UsbDevice; //importé manuellement
import android.hardware.usb.UsbDeviceConnection; //importé manuellement
import android.hardware.usb.UsbManager; //importé manuellement
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/*====================================================================================
- l'utilisateur doit brancher le lecteur RFID "DLP RFID2D" à la tablette
- une fois branché, l'utilisateur peut scanner la donner contenue dans un tag proche
=====================================================================================*/

public class MainActivity extends AppCompatActivity {

    /*========================================================
        déclaration des éléments graphiques pour la lecture
     ========================================================*/
    Button mScanBtn;
    Button mCopyBtn;
    TextView plantId;
    UsbManager usbManager;
    UsbDevice device;
    UsbSerialDevice serialPort;
    UsbDeviceConnection connection;

    /*========================================================
        déclaration des éléments graphiques pour l'écriture
     ========================================================*/
    Button mWriteBtn;
    EditText editLieux,editEspece,editNumLot,editNumEspece;

    //========variable boolean qui indiquera si nous sommes en mode écriture ou non=========
    Boolean ecrire = false;

    //======variables qui vont contenir nos blocs hexadecimaux==========
    String str_blocks[] = new  String[4]; //pour nos 4 blocs de texte saisie
    String hexa_blocks[] = new String[4];


    private static final String ACTION_USB_PERMISSION =
            "com.android.example.USB_PERMISSION";

    UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback() { //Defining a Callback which triggers whenever data is read.
        @Override
        public void onReceivedData(byte[] arg0) {
            String data;
            try {
                data = new String(arg0, "UTF-8");
                data.concat("/n");

                afficher(plantId,data);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    };



    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() { //Broadcast Receiver pour lancer et arréter automatiquement la communication série.
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public void onReceive(Context context, Intent intent) { //lorsqu'un événement est capturé

            if (intent.getAction().equals( ACTION_USB_PERMISSION)) { //si cet événement est une demande de permission usb
                boolean granted = intent.getExtras().getBoolean(UsbManager.EXTRA_PERMISSION_GRANTED); //on récupére la réponse à cette demande permission ("vrai" ou "faux")
                //si la permission est accordée par l'utilisateur:

                if (granted) {
                    connection = usbManager.openDevice(device); //on instantie notre connexion
                    serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection); //on instantie le port série sur lequel est relié le lecteur RFID
                    if (serialPort != null) { //si le port est bien créer (donc si il existe)
                        if (serialPort.open()) { //On ouvre la connexion série à travers ce port avec les parmètre définis juste en dessous.

                            serialPort.setBaudRate(115200); //la vitesse de communication entre l'appareil android et le lecteur doit être de 115200 bauds
                            serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8); //les données transmises seront ségmentées en octets
                            serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1); //un bit de stop
                            serialPort.setParity(UsbSerialInterface.PARITY_NONE); //pas de bit de parité
                            serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF); //pas de controle de flux

                            if(!ecrire) {
                                //scan de la présence de tag RFID aux alentours du lecteur
                                String string[] = new String[3];

                                /*==============================================
                                    Commande de lecture des 4 blocs de donnees
                                =============================================== */

                                //string[0] = "010C00030410002101000000"; //Register write request
                                string[0] = "010C00030410003101000000";
                                string[1] = "01080003042B0000"; //enable external antenna
                                string[2] = "010C00030418402300030000"; //Read 3 blocks (test)
                                //string[2] = "010C00030418402300030000"; //Read 3 blo
                                for (int i = 0; i < 3; i++) { // on envoi les trois requètes
                                    serialPort.write(string[i].getBytes());
                                    try {
                                        Thread.sleep(800);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                                serialPort.read(mCallback);
                                //Toast.makeText(getApplicationContext(), "Connexion série établie", Toast.LENGTH_SHORT).show();

                            }
                            else {
                                //=====ce que nous faisons si nous souhaitons écrire=======:
                                String commande_ecriture[]= new String[7];
                                commande_ecriture[0] = "010C00030410002101000000"; //Register write request
                                commande_ecriture[1] = "01080003042B0000"; //Activation antenne externe
                                //la commande suivante contient le flag "option" pour lancer l'écriture, cependant celui ci n'est pas pris en charge par les clous rfid, nous devons donc nous adapter en ne mettant pas ce flag en plac 0x40 --> 0x00
                                //commande_ecriture[3] = "010F0003041840210"; //préfixe commanded d'écriture
                                commande_ecriture[3] = "010F0003041800210"; //préfixe commanded d'écriture sans flag option
                                commande_ecriture[4] = ""; //contiendra le numéro de bloc
                                commande_ecriture[5] = ""; //contiendra la donnée à écrire
                                commande_ecriture[6] = "0000"; //suffixe de la commande d'écriture

                                /*====================================================================
                                on simule que nous avons déjà obtenu nos 4 bloc hexa des champs texte
                                 ===================================================================*/


                                /*hexa_blocks[0] = "5341534F";
                                hexa_blocks[1] = "50495453";
                                hexa_blocks[2] = "454E3939";
                                hexa_blocks[3] = "39393830";*/
                                //pour obtenir : SASOPITSEN999900


                                //envoie des premières commandes
                                for(int i=0;i<2;i++){
                                    serialPort.write(commande_ecriture[i].getBytes());
                                    try {
                                        Thread.sleep(800);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }

                                //envoie des commandes d'écriture:
                                for(int j=0;j<4;j++){
                                    commande_ecriture[4] = Integer.toString(j);
                                    commande_ecriture[5] = hexa_blocks[j];
                                    serialPort.write((commande_ecriture[3]+commande_ecriture[4]+commande_ecriture[5]+commande_ecriture[6]).getBytes());
                                    try {
                                        Thread.sleep(800);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            activerBtn(true); //on active le bouton de scan






                        } else {
                            Toast.makeText(getApplicationContext(), "Port occupé", Toast.LENGTH_SHORT).show();
                            //serialPort.close();
                            //demandePermission();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Permission null", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Permission non accordée", Toast.LENGTH_SHORT).show();

                }
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) { //si le lecteur connecté à l'appareil android
                Toast.makeText(getApplicationContext(), "lecteur branché", Toast.LENGTH_SHORT).show(); //on l'affiche à l'utilisateur
                //demandePermission();
                activerBtn(true); //on active le bouton de scan
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) { //si le lecteur connecté de l'appareil android
                Toast.makeText(getApplicationContext(), "lecteur débranché", Toast.LENGTH_SHORT).show(); //on l'affiche aussi
                activerBtn(false); //on désactive le bouton de scan

            }
        }
    };

    public void activerBtn(boolean activ){
        mScanBtn.setEnabled(activ);
        mWriteBtn.setEnabled(activ);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usbManager = (UsbManager) getSystemService(USB_SERVICE);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(broadcastReceiver, filter);

        //on instancie nos composants:
        editLieux = (EditText) findViewById(R.id.lieux_txt);
        editEspece = (EditText) findViewById(R.id.espec_txt);
        editNumLot = (EditText) findViewById(R.id.num_lot_txt);
        editNumEspece = (EditText) findViewById(R.id.num_individu_txt);

        //action du bouton d'actualisation
        mScanBtn = (Button)findViewById(R.id.scanBtn);


        //récupération du text contenu dans le champ identifiant
        plantId = (TextView) findViewById(R.id.idText);
        
        mScanBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //===========lancement du scan========================
                activerBtn(false); //on désactive le bouton de scan
                ecrire = false;
                demandePermission();
                //====================================================

            }
        });


        //action du bouton de copie
        mCopyBtn = (Button)findViewById(R.id.copierBtn);
        mCopyBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //récupération du presse papier et préparation de la donnée à sauvegarder dans le presse papier
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(null, plantId.getText());
                //copie du text dans le presse-papier
                if (clipboard != null) {
                    clipboard.setPrimaryClip(clip);
                }

            }
        });

        //action du bouton d'écriture
        mWriteBtn = (Button)findViewById(R.id.write_btn);
        mWriteBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                //===========récupération des données saisies dans les champs textes=======
                if(editLieux.getText().length()==4 && editEspece.getText().length()==6 && editNumLot.getText().length()==3 && editNumEspece.getText().length()==3) {
                    str_blocks[0] = editLieux.getText().toString();
                    str_blocks[1] = editEspece.getText().subSequence(0, 4).toString();
                    str_blocks[2] = editEspece.getText().subSequence(4, 6).toString() + editNumLot.getText().subSequence(0, 2).toString();
                    str_blocks[3] = editNumLot.getText().subSequence(2, 3).toString() + editNumEspece.getText().toString();

                    for(int i=0;i<4;i++){
                        hexa_blocks[i]=asciiToHex(str_blocks[i]);
                    }
                    //===========lancement du scan========================
                    activerBtn(false); //on désactive le bouton de scan
                    ecrire =true;
                    demandePermission();
                    //====================================================
                }else {
                    Toast.makeText(getApplicationContext(), "Champs mal remplis", Toast.LENGTH_SHORT).show();
                }



            }
        });


        //plantId.setMovementMethod(new ScrollingMovementMethod());
    }

    //fonction de demande de permission usb
    public void demandePermission(){

        HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList(); //on crée une "map" contenant la liste des appareils usb connectés à l'appareil android.
        if (!usbDevices.isEmpty()) {
            boolean keep = true;
            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) { //on regarde chaque ligne de la "map"
                device = entry.getValue(); // on définit notre appareil comme la valeur contenu dans la ligne actuelle de la "map"
                int deviceVID = device.getVendorId(); // on récupére l'ID du vendeur de l'appareil connecté
                if (deviceVID == 1027)//RFID2D Vendor ID (si l'ID du vendeur correspond)
                {
                    PendingIntent pi = PendingIntent.getBroadcast(getApplicationContext(), 0,
                            new Intent(ACTION_USB_PERMISSION), 0); //On démare la procédure de demande de permission à l'utilisateur
                    usbManager.requestPermission(device, pi);
                    keep = false; //on arrète de chercher
                } else {
                    connection = null; //si l'ID du vendeur n'est pas le bon on considére que rien n'est connecté
                    device = null; //si l'ID du vendeur n'est pas le bon on considère que l'appareil n'est pas connecté et n'a donc pas de nom
                }

                if (!keep)
                    break; // on passe à la suite du programme
            }
        }
    }
    //fonction d'affichage
    private void afficher(TextView tv, final CharSequence text) {
        final TextView ftv = tv;
        final String result = text.toString();
        final int resLength = text.length();
        final String caractereHexaNonTrie = result.substring(resLength-39, resLength-31) + result.substring(resLength-29,resLength-21) + result.substring(resLength-19,resLength-11) + result.substring(resLength-9,resLength-1);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                /*==================================================
                *                       resultat 1
                * ================================================*/
                //nous trions la donnée ici
                if (resLength > 44) {
                    //on réinitialise la text view dans laquelle s'affiche l'identifiant de la plante
                    ftv.setText(null);

                    if(result.contains("[]")){
                        ftv.append("AUCUN TAG DETECTE");
                    }
                    else {
                        /*===================================================
                        *                   affichage du résultat
                         ===================================================*/
                        String chaineLisible;
                        chaineLisible = hexToAscii(caractereHexaNonTrie);
                        ftv.append(chaineLisible);

                    }
                }else {
                    ftv.setText(null);
                    ftv.append("ERREUR DE LECTURE");
                }
            }
        });

    }
    private static String hexToAscii(String hexStr){
        StringBuilder output = new StringBuilder("");

        for (int i = 0; i < hexStr.length(); i += 2) {
            String str = hexStr.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }

        return output.toString();
    }
    /*=============================================================
                    Convertisseur asciihexa
    ============================================================ */
    private static String asciiToHex(String asciiStr){
        char[] ch = asciiStr.toCharArray();
       StringBuilder hexOutput = new StringBuilder("");

       for (char c : ch){
           int i = (int) c;
           hexOutput.append((Integer.toHexString(i).toUpperCase()));
       }
       return hexOutput.toString();
    }




    /*=======================================================
                on gére la pause de l'application
     =======================================================*/
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}
