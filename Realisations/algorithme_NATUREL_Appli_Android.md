# Algorithme en langage naturel

### ===== DÉBUT =====

#### Lancer un scan RFID

QUAND le bouton "Scanner" est appuyé :
   envoyer une requéte de scan
   
TANT QU'il n'y a pas de tag trouvé :
    griser le bouton "Scanner"
FIN TANT QUE

Dé-griser le bouton "Scanner"


#### Ne considérer que le tag RFID le plus proche

Comparer les puissances reçues

Ne prendre en compte que le tag qui émet le plus fort

Stopper le scan


#### Récupérer la donnée lue

Récupérer les informations du tag le plus fort


#### Trier les informations reçues

Extraire les infos utiles (l'UID) et les stocker dans une variable de type 'chaine de caractère'


#### Afficher l'UID du tag lu

Afficher l'UID

QUAND le bouton "Copier" est appuyé :
    Copier l'UID dans le presse-papier d'Android

### ===== FIN =====